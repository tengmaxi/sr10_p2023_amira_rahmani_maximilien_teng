var express = require('express');
var router = express.Router();
var candModel = require('../model/candidature.js');
var offreModel = require('../model/offres.js');
var db = require('../model/db.js');
const candidature = require('../model/candidature.js');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});


router.get('/candidatureslist', function (req, res, next) {
  result=candModel.readall(function(result){
    res.render('candidaturesList', { title: 'Liste des candidats', candidats:result });
});});


router.get('/mesCandidatures', function (req, res, next) {
  result=candModel.readallbyemail(req.session.userid,function(result){
  res.render('mesCandidatures', { title: 'Liste des candidats', candidats:result });
});});
module.exports = router;