var express = require('express');
var router = express.Router();
var offreModel = require('../model/offres.js');
var ficheposteModel = require('../model/ficheposte.js');

/* GET offres listing. */
router.get('/offreslist', function (req, res, next) {
  result=ficheposteModel.readallOffreFiche(function(result){
    res.render('offres', { title: 'Liste des offres', offres : result });
  });});

  router.get('/detailFiche', function (req, res, next) {
    const idFiche = req.query.idFiche; // Retrieve the value of idFiche from the URL parameter
  
    result = offreModel.readFichebyOffre(idFiche, function (result) {
      res.render('fichesposteList', { title: 'Détails de cette offre', fiches: result });
    });
  });

/*Get offres emplois */
router.get('/', function (req, res, next) {
  result=offreModel.readOffreFiche(function(result){
    // Récupération de la valeur de tri sélectionnée dans le formulaire
	  const tri = req.query.sort;
    // Tri des offres par date de publication
    if (tri == 'date') {
      result.sort((a, b) => {
        const dateA = new Date(a.datePublication);
        const dateB = new Date(b.datePublication);
        return dateB - dateA; // Tri décroissant par date de publication
      });
    }    
    // Rendu de la page des offres avec les données des offres triées
	  res.render('offres', { offres: result });
  });});
  
module.exports = router;
