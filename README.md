Résumé :
Notre projet est une application web, un site internet, avec trois types de sessions possibles :
admin, recruteur et candidat. Lors de l'inscription d'un nouvel utilisateur, celui-ci est
automatiquement défini en tant que candidat par défaut.
Nous avons organisé les routes de notre application de manière à ce que chaque action
réalisée à partir de la barre de navigation soit dirigée vers des routes spécifiques en fonction
du type d'utilisateur. Les routes correspondantes aux actions des utilisateurs sont
regroupées sous le nom du type d'utilisateur, à l'exception des routes pour les actions des
candidats, qui ont été mises en place au début du développement du site sans une approche
claire.
Nous avons adopté une approche progressive pour développer les fonctionnalités
spécifiques à chaque type d'utilisateur. Cela peut être une bonne façon de procéder lorsque
nous explorons une nouvelle technologie ou que nous ne disposons pas encore d'une vision
claire de toutes les fonctionnalités nécessaires.
Notre approche est également bien conçue pour être réutilisable. Si nous souhaitons
changer le design, cela se fait simplement en modifiant les vues sans avoir à modifier la
logique sous-jacente.
De plus, l'ajout de nouvelles actions se fait simplement en ajoutant une nouvelle route
correspondante et en adaptant la vue associée. Cela montre que nous avons structuré votre
code de manière modulaire et extensible. En ajoutant de nouvelles routes et en adaptant les
vues, nous pouvons facilement intégrer de nouvelles fonctionnalités à votre application sans
perturber les fonctionnalités existantes.
Une architecture réutilisable et modulaire présente de nombreux avantages, tels que la
facilité de maintenance, la réduction des duplications de code et la possibilité d'ajouter de
nouvelles fonctionnalités de manière efficace. Nous avons pris en compte ces principes dans
la conception de notre application web, ce qui est une approche positive pour assurer la
flexibilité et l'évolutivité de notre projet
Dans notre application nous n’avons pas eu le temps de mettre en place les filtres pour les
offres.Les filtres peuvent être un aspect complexe à développer, car ils nécessitent souvent
une logique avancée pour trier et filtrer les données de manière efficace.
Voici une approche générale pour implémenter des filtres pour les offres dans notre
application web :
• Analyse des besoins :
Identifiez les critères de filtrage pertinents pour les offres. Cela pourrait inclure des
catégories, des localisations, des mots-clés, des niveaux d'expérience, des types de contrat,
etc.
• Conception de l'interface utilisateur :
Ajoutez des éléments d'interface utilisateur, tels que des menus déroulants, des cases à
cocher, des champs de recherche, etc., permettant aux utilisateurs de sélectionner les
critères de filtrage souhaités.
• Implémentation des routes :
Ajouter une nouvelle route dans notre application pour gérer les requêtes de filtrage des
offres.
Cette route devrait recevoir les paramètres de filtrage sélectionnés par l'utilisateur.
• Traitement des données :
Dans la logique de notre route de filtrage, nous utilisons les paramètres reçus pour
construire une requête à votre base de données.
Utilisez les critères de filtrage pour générer une requête SQL appropriée qui récupère les
offres correspondantes.
• Affichage des résultats :
Renvoyez les offres filtrées à la vue correspondante pour les afficher à l'utilisateur.
• Mise à jour de la vue :
Utiliser des techniques de mise à jour comme on a fait pour la session Admin.
Nous pouvons également permettre à l’Admin de recevoir et accepter les demandes pour la
création d’une nouvelle organisation. Nous avons déjà un code similaire, donc il suffit juste
de le reprendre et de l’adapter aux informations nécessaires. 

Tests unitaires : 

ont peut voir le résultat des tests en faisant "npm run test". Ici, une partie des fonctions administrateur ont été
testées.