var db = require('./db.js');
module.exports = {

    //affiher les type de metiers de notre base de données
    readTypeMetier: function (callback) {
        db.query("select distinct fp.typePoste from FichePoste fp", function
            (err, results) {
            if (err) throw err;
            callback(results);
        });
    },

    //afficher le poste d'un id
    read: function (id, callback) {
        db.query("select * from FichePoste where idPoste= ?", id, function
            (err, results) {
            if (err) throw err;
            callback(results);
        });
    },

    //afficher les postes d'une lieu (-> utile lorsque le candidat cherche des postes par lieu) 
    readbyLieu: function (lieu, callback) {
        db.query("select * from FichePoste where lieu= ?", lieu, function
            (err, results) {
            if (err) throw err;
            callback(results);
        });
    },

    //afficher les postes d'une typeMetier (-> utile lorsque le candidat cherche des postes par typeMetier) 
    readbyMetier: function (typeMetier, callback) {
        db.query("select * from FichePoste where typePoste= ?", type, function
            (err, results) {
            if (err) throw err;
            callback(results);
        });
    },

    readall: function (callback) {
        db.query("select * from FichePoste", function (err, results) {
            if (err) throw err;
            callback(results);
        });
    },

    readallOffreFiche: function (callback) {
        db.query("SELECT * FROM FichePoste JOIN Offres ON FichePoste.id_fiche = Offres.id_fiche JOIN Organisation ON Offres.Siren = Organisation.Siren;", function (err, results) {
            if (err) throw err;
            callback(results);
        });
    },
    // areValid: function (email, password, callback) {
    //     sql = "SELECT motdepasse FROM Users WHERE email = ?";
    //     rows = db.query(sql, email, function (err, results) {
    //         if (err) throw err;
    //         if (rows.length == 1 && rows[0].pwd === password) {
    //             callback(true)
    //         } else {
    //             callback(false);
    //         }
    //     });
    // },
    // creat: function (email, nom, prenom, pwd, type, callback) {
    //     //todo
    //     return false;
    // }
}


