var express = require('express');
var router = express.Router();

// Define routes
router.get('/getInfo', (req, res) => {
    res.render('apropos'); 
});

module.exports = router;
