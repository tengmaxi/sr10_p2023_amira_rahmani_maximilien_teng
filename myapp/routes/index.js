var express = require('express');
var router = express.Router();
const mysql = require('mysql');
var session= require('../session');
const path = require('path');



var conn = mysql.createConnection({
	host: "tuxa.sme.utc", //ou localhost
    user: "sr10p043", //"ai16p002",
    password: "srb24qGWtSzA",//'89PRl9zx2fnh',
    database: "sr10p043"
});

router.use(express.json());
router.use(express.urlencoded({ extended: true }));
router.use(express.static(path.join(__dirname, 'static')));


// http://localhost:3000/
router.get('/', function(request, response) {
	// Render login template
  response.render('index', { title: 'Express' });
});

// http://localhost:3000/auth
// Add a variable to keep track of the number of unsuccessful login attempts
let loginAttempts = 0;

// Add a variable to store the timestamp of the last unsuccessful login attempt
let lastLoginAttemptTime = null;

router.post('/auth', function(request, response) {
  // Capture the input fields
  let email = request.body.email;
  let password = request.body.password;
  
  // Check if the user has exceeded the maximum number of login attempts
  if (loginAttempts >= 5) {
    // Check if enough time has passed since the last unsuccessful login attempt (e.g., 5 minutes)
    const currentTime = Date.now();
    const timeSinceLastLoginAttempt = currentTime - lastLoginAttemptTime;
    const fiveMinutesInMillis = 5 * 60 * 1000; // 5 minutes in milliseconds

    if (timeSinceLastLoginAttempt < fiveMinutesInMillis) {
      const remainingTime = Math.ceil((fiveMinutesInMillis - timeSinceLastLoginAttempt) / 1000);
      response.send(`Trop d'essais infructueux, réesayez dans ${remainingTime} secondes.`);
      return;
    } else {
      // Reset the login attempts counter and update the last login attempt time
      loginAttempts = 0;
      lastLoginAttemptTime = null;
    }
  }

  // Ensure the input fields exist and are not empty
  if (email && password) {
    // Execute SQL query that'll select the account from the database based on the specified email and password
    conn.query('SELECT * FROM Users WHERE email = ? AND motdepasse = ?', [email, password], function(error, results, fields) {
      // If there is an issue with the query, output the error
      if (error) throw error;
      
      // If the account exists
      if (results.length > 0) {
        // Authenticate the user
        session.creatSession(request.session, email, results[0].type, results[0].prenom, results[0].nom);
        
        // Redirect to the appropriate page based on user type
        if (results[0].type === 'candidat') {
          console.log("c'est un candidat");
          response.redirect('/users');
        } else if (results[0].type === 'recruteur') {
          console.log("c'est un recruteur");
          response.redirect('/recruteur');
        } else if (results[0].type === 'admin') {
          console.log("c'est un admin");
          response.redirect('admin/userslist');
        }
      } else {
        // Increment the login attempts counter and update the last login attempt time
        loginAttempts++;
        lastLoginAttemptTime = Date.now();
        response.send('Incorrect Username and/or Password!');
      }
      
      response.end();
    });
  } else {
    response.send('Please enter Username and Password!');
    response.end();
  }
});

// http://localhost:3000/home
router.get('/home', function(request, response) {
	// If the user is loggedin
	if (request.session.loggedin) {
		// Output username
		response.redirect('/users');
	} else {
		// Not logged in
		response.send('Please login to view this page!');
	}
	response.end();
});


router.get('/inscription', function(req, res, next) {
  res.render('inscription');
});


module.exports = router;
