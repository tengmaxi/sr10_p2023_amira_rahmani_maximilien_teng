var db = require('./db.js');
module.exports = {
    read: function (id, email, callback) {
        db.query("select * from Candidature where idOffre= ? AND email = ?", id, email, function
            (err, results) {
            if (err) throw err;
            callback(results);
        });
    },
    readall: function (callback) {
        db.query("select * from Candidature", function (err, results) {
            if (err) throw err;
            callback(results);
        });
    },
    readallbyemail: function (email, callback) {
        db.query("select of.*, fp.*, cand.* from Candidature cand , Offres of, FichePoste fp where cand.idOffre = of.idOffre and of.id_fiche = fp.id_fiche and cand.email = ?", [email], function
            (err, results) {
            if (err) throw err;
            callback(results);
        });
    },
    readbyEmailIdOffre : function (email,idOffre,callback) {
        db.query("select * from Candidature where email = ? and idOffre = ?",[email,idOffre], function (err, results) {
            if (err) throw err;
            callback(results);
        });
    },

    readbyEmailIdFiche: function(email,idFiche,callback){
        db.query("select * from Candidature c, Offres of where of.idOffre=c.idOffre and email = ? and id_fiche = ?",[email,idFiche], function (err, results) {
            if (err) throw err;
            callback(results);
        });
    },

    readbyOrga: function(req, callback) {
        db.query("SELECT Siren FROM Users WHERE email = ?;", [req.session.userid], function(err, results) {
          if (err) throw err;
          var siren = results[0].Siren;
          db.query("SELECT * FROM Candidature WHERE orga = ?;", [siren], function(err, results) {
            if (err) throw err;
            callback(results);
          });
        });
      },

    readbyOffre: function (callback) {
        db.query("select of.*, cand.*, orga.*, from Candidature cand, Offres of, Organisation orga, where cand.idOffre = of.idOffre and orga.Siren = of.Siren", function
            (err, results) {
            if (err) throw err;
            callback(results);
        });
    },
}

