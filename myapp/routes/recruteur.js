var express = require('express');
var router = express.Router();
var offresModel = require('../model/offres.js');
var candModel = require('../model/candidature.js');
var userModel = require('../model/users.js');
var ficheModel = require('../model/ficheposte.js');
var demandeModel = require('../model/demandes.js');
var db = require('../model/db.js');

/*
var userModel = require('../model/offres.js');
var offresModel = require('../model/offres.js');
var ficheModel = require('../model/ficheposte.js');
var candModel = require('../model/candidature.js');
var orgaModel = require('../model/organisations.js');
*/

router.get('/', function(req, res, next) {
  console.log("router.get('/') de recruteur.js")
  res.render('accueilRecruteur');
});

router.get('/mesOffres', function(req,res,next){
    console.log(req.session.userid)
    offresModel.readbyRecruteur(req.session.userid,function(result){
    res.render('mesOffres',{offres: result});
})
});

/*router.get('/candidats', function(req,res){
    candModel.readbyOffre(req.query.idOffre,function(result){
    res.render('candidats',{candidats: result})
  })
});*/

router.get('/candidats', function(req, res, next) {
  // Utilisez candModel.readbyOrga en lui passant req et une fonction de rappel
  candModel.readbyOrga(req, function(result) {
    res.render('candidats', { title: 'Liste des utilisateurs demandant à rejoindre votre organisation', candidats: result });
  });
});

router.get('/newOffre', function(req,res){
  userModel.readbyEmail(req.session.userid,function(result){
  res.render('nouvelleOffre',{recruteur : result})
  })
});

router.get('/newFiche', function(req,res){
  userModel.readbyEmail(req.session.userid,function(result){
  res.render('nouvelleFiche',{recruteur : result})
  })
});

router.get('/recruteurdem', function (req, res, next) {
  demandeModel.readallusersenattente(req, function (result) {
    res.render('demandesTraitement', { title: 'Liste des utilisateurs demandant à rejoindre votre organisation', demandes: result });
  });
});

router.post('/updateJoin', function (req, res) {
  var emailToAdd = req.body.emailA;
  var siren = req.body.sirenA;
  demandeModel.updateToJoin(emailToAdd, siren, function (result) {
      //if (err) throw err;

  });
  demandeModel.deleteByEmail(emailToAdd, function (result) {
    //if (err) throw err;


});
res.redirect('./recruteurdem');

});

router.post('/deleteDem', function (req, res) {
  var demToJoin = req.body.demToJoin;

  demandeModel.deleteByEmail(demToJoin, function (result) {
      //if (err) throw err;


  });
  res.redirect('./recruteurdem');

});
  
router.get('/fichespostelist', function (req, res, next) {
  result=ficheModel.readall(function(result){
    res.render('listAllFichesPoste', { title: 'Liste des fiches de poste', fiches:result });
  });});

  router.post('/addFiche', function (req, res, next) {
    const statut = req.body.statutposte;
    const description = req.body.descriptionPoste;
    const type = req.body.typePoste;
    const responsable = req.body.rh;
    const intitule = req.body.intitule;
    const salaire = req.body.salaire;
    const horaire = req.body.horaire;
    const lieu = req.body.lieu;
  
    sql = "INSERT INTO FichePoste (statutposte, rh, typePoste, intitule, salaire, lieu, descriptionPoste, horaire) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    
    var ficheToInsert = [statut, responsable, type, intitule, salaire, lieu, description, horaire];
  
    rows = db.query(sql, ficheToInsert , function (err, results) {
            if (err) throw err;
            res.render('displayRecruteur', { message: 'Votre fiche a été ajoutée avec succès'});

        });   
    });

    router.post('/addOffre', function (req, res, next) {
      const idFiche = req.body.id_fiche;
      const description = req.body.desc;
      const dateVal = req.body.dateVal;
      const datePub = req.body.datePub;
      const recruteur = req.body.recruteur;
      const etat = "Publiee"
      const siren = req.body.siren;
    
      sql = "INSERT INTO Offres (etat, datePublication, dateValidite, recruteur, id_fiche, description, siren) VALUES (?, ?, ?, ?, ?, ?, ?)";
      
      var ficheToInsert = [etat, datePub, dateVal, recruteur, idFiche, description, siren];
    
      rows = db.query(sql, ficheToInsert , function (err, results) {
              if (err) throw err;
              res.render('displayRecruteur', { message: 'Votre Offre a été ajoutée avec succès'});
  
          });   
      });

module.exports = router;