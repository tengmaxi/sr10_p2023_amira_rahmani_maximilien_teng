var express = require('express');
var router = express.Router();
var userModel = require('../model/users.js');
var offresModel = require('../model/offres.js');
var ficheModel = require('../model/ficheposte.js');
var demandesModel = require('../model/demandes.js');
var orgaModel = require('../model/organisations.js');
var db = require('../model/db.js');
var sess = require('../session.js');

// Authorization middleware
function authorize(role) {
    return function(req, res, next) {
      // Check if the user has the required role or permissions
      if (req.session.role === role) {
        // User has the required role, allow access to the route
        next();
      } else {
        // User does not have the required role, redirect or show an error message
        res.status(403).send('Unauthorized Access');
      }
    };
  }

router.get('/demandesList', authorize('admin'), function (req, res, next) {
    demandesModel.readall(function (result) {
        res.render('demandesList', { title: 'Liste des demandes', demandes: result });
    });
});

router.get('/userslist', authorize('admin'), function (req, res, next) {
    userModel.readall(function (result) {
        orgaModel.readall(function (org) {
            res.render('usersList', { title: 'Liste des utilisateurs', users: result, orgas: org });
        });
    });
});

router.get('/usersAdmin', authorize('admin'), function (req, res, next) {
    userModel.readall(function (result) {
        orgaModel.readall(function (org) {
            res.render('usersAdmin', { title: 'Liste des utilisateurs', users: result, orgas: org });
        });
    });
});

router.get('/orgaAdmin', function (req, res, next) {
    result=orgaModel.readall(function(result){
      res.render('orgaAdmin', { title: 'Liste des organisations', orgas:result });
    });});

    router.post('/deleteOrga', function (req, res) {
        var sirenToDelete = req.body.orgaToDelete;

        orgaModel.deleteBySiren(sirenToDelete, function (result) {
            //if (err) throw err;


        });
        res.redirect('./orgaAdmin');

    });

router.get('/recruteursList', authorize('admin'), function (req, res, next) {
    userModel.readall(function (result) {
        var recruteurs = result.filter(function (user) {
            return user.type === 'recruteur';
        });

        res.render('recruteursList', { title: 'Liste des recruteurs', recruteurs: recruteurs });
    });
});

router.post('/deleteUser', authorize('admin'), function (req, res) {
    var emailToDelete = req.body.emailToDelete;

    userModel.deleteByEmail(emailToDelete, function (result) {
        //if (err) throw err;


    });
    res.redirect('./userslist');

});

router.post('/addAdmin', authorize('admin'), function (req, res) {
    var emailToAdmin = req.body.emailToAdmin;
    userModel.updateToAdmin(emailToAdmin, function (result) {
        //if (err) throw err;

    });
    res.redirect('./usersAdmin');

});

module.exports = router;
