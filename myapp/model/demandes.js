var db = require('./db.js');
module.exports = {
    //afficher toutes les demandes d'un utilisateur
    read: function (email, callback) {
        db.query("select * from Demande where emailusers= ?", email, function
            (err, results) {
            if (err) throw err;
            callback(results);
        });
    },

    //afficher toutes les demandes des utilisateurs pour une organisation
    readUsers : function(orga, callback) {
        db.query("select * from Demande where orga= ?", orga, function
        (err,results){
            if (err) throw err;
            callback(results);
            
        });
    },

    readall: function (callback) {
        db.query("select * from Demande", function (err, results) {
            if (err) throw err;
            callback(results);
        });
    },

    readallusersenattente: function (req,callback, result) {
        db.query("select Siren from Users WHERE email = ? ;",[req.session.userid], function (err, results) {
            if (err) throw err;
            var siren = results[0].Siren;
            db.query("select * from Demande WHERE orga =  ?;", [siren], function (err, results) {
                if (err) throw err;
                callback(results);
            });
        });

    },

    updateToJoin: function (email, siren, callback) {
        var sql = "UPDATE Users SET type = 'recruteur', Siren = ? WHERE email = ?";
        db.query(sql, [siren, email], function (err, results) {
          if (err) throw err;
          callback(results);
        });
      },

    readActive: function (callback) {
        db.query("select * from Demande where traitement = 0", function (err, results) {
            if (err) throw err;
            callback(results);
        });
    },

    deleteByEmail: function (email, callback) {
        var sql = "DELETE FROM Demande WHERE emailUser = ?";
        db.query(sql, [email], function (err, results) {
            if (err) throw err;
            callback(results);
        });
    },
}

