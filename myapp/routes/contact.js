var express = require('express');
var router = express.Router();

// Define routes
router.get('/getContact', (req, res) => {
    res.render('contact'); 
});

module.exports = router;
